package com.devcamp.hellodevcampworld.api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@RestController
public class CPizzaCampaign {
    @CrossOrigin
    @GetMapping("/devcamp-simple")
    public String simple() {
        return "test campaign";
    }

    @CrossOrigin
    @GetMapping("/devcamp-date")
    public String getDateViet() {
        DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hello pizza lover ! Hôm nay %s mua 1 tặng 1.", dtfVietnam.format(today));
    }
    @CrossOrigin
    @GetMapping("/devcamp-lucky-number")
    public String getLuckyNumber(@RequestParam(name = "username") String username) {
        int randomIntNumber = 1 + (int) (Math.random() * (6 - 1 +1 ));
        return String.format("Xin chào %s! Số may mắn hôm nay của bạn là %s !!!", username, randomIntNumber);
    }
}


